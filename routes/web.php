<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::middleware(['auth'])->group(function () {

    Route::get('/home', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::get('/logout', function () {
        \Illuminate\Support\Facades\Auth::logout();
        return redirect(\route('welcome'));
    })->name('logout');


    /**
     * User
     */
    Route::get('/user/{user}/edit', [\App\Http\Controllers\UserController::class, 'edit'])->name('user.edit');

    Route::put('/user/{user}/update', [\App\Http\Controllers\UserController::class, 'update'])->name('user.update');

    /**
     * Cars
     */
    Route::get('/user/{user}/cars', [\App\Http\Controllers\CarController::class, 'index'])->name('cars.index');

});


