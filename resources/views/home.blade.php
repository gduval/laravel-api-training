@extends('layout.app') @section('content')

    <div>
        <h1 class="title mt-6">
            Hello, <b>{{ $user->getName() }}</b> !
        </h1>

        <div class="card my-5">
            <div class="card-content">
                <div class="content">
                    <p>User token: <b>{{ $user->tokens()->first()->token }}</b></p>
                    <a href="#">Learn more about token</a>
                </div>
            </div>
        </div>
    </div>

    @foreach($user->cars as $car)
        Vroom
    @endforeach

@endsection
