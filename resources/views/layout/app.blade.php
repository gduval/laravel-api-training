<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hello Bulma!</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
<section class="section">
    <div class="container">

        @if (session()->has('success'))
            <article class="message is-success">
                <div class="message-header">
                    <p>Success</p>
                </div>
                <div class="message-body">
                    {{ session()->get('success') }}
                </div>
            </article>
        @endif

        @auth()
            @include('component.main-nav')
        @endauth

        @yield('content')

    </div>
</section>

<script src="{{ asset('js/app.js') }}"></script>

</body>
</html>
