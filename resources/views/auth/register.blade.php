@extends('layout.app')

@section('content')

    <nav class="breadcrumb" aria-label="breadcrumbs">
        <ul>
            <li><a href="/">Home</a></li>
            <li class="is-active"><a href="#" aria-current="page">Register</a></li>
        </ul>
    </nav>

    <h1 class="title">
        Register
    </h1>
    <p class="subtitle">
        Create your account.
    </p>

    <div class="card my-5">
        <div class="card-content">
            <div class="content">
                <p>You already have an account ?</p>
                <a href="/login" title="Create my account">Login with your account.</a>
            </div>
        </div>
    </div>

    <form action="/register" method="POST">

        @csrf

        <div class="field">
            <label class="label">Name*</label>
            <div class="control">
                <input class="input" type="text" name="name" placeholder="Enter your name" value="{{ old('name') }}">
            </div>
            @error('name')
            <p class="help is-danger">{{ $message }}</p>
            @enderror
        </div>

        <div class="field">
            <label class="label">Email address*</label>
            <div class="control">
                <input class="input" type="email" name="email" placeholder="your@email.com" value="{{ old('email') }}">
            </div>
            @error('email')
            <p class="help is-danger">{{ $message }}</p>
            @enderror
        </div>

        <div class="field">
            <label class="label">Password*</label>
            <div class="control">
                <input class="input" type="password" name="password"
                       placeholder="Your password must be at least 8 characters long">
            </div>
            @error('password')
            <p class="help is-danger">{{ $message }}</p>
            @enderror
        </div>

        <div class="field">
            <label class="label">Confirm your password*</label>
            <div class="control">
                <input class="input" type="password" name="password_confirmation" placeholder="Text input">
            </div>
        </div>

        <div class="field is-grouped">
            <div class="control">
                <button class="button is-link">Submit</button>
            </div>
        </div>
    </form>

@endsection
