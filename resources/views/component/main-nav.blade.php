<nav id="navbar" class="bd-navbar navbar">
    <div class="navbar-brand">
        <a class="navbar-item" href="{{ route('home') }}">
            <img src="https://bulma.io/images/bulma-logo.png"
                 alt="Bulma: Free, open source, and modern CSS framework based on Flexbox" width="112" height="28"/>
        </a>
    </div>
    <div id="navMenuDocumentation" class="navbar-menu">
        <div id="navbarStartOriginal" class="navbar-start bd-navbar-start bd-is-original">
            <a class="navbar-item bd-navbar-item bd-navbar-item-expo bd-navbar-item-base {{ \Illuminate\Support\Facades\Route::is('cars.index') ? 'is-active' : ''  }}"
               href="{{ route('cars.index', $user) }}">
                    <span class="icon has-text-primary">
                        <i class="fas fa-car"></i>
                    </span>
                    <span class="is-hidden-tablet-only is-hidden-desktop-only">
                        My Cars
                    </span>
            </a>

            <div class="navbar-item bd-navbar-item bd-navbar-item-base has-dropdown is-hoverable">
                <a class="navbar-link bd-navbar-ellipsis">
                    <span class="icon">
                        <i class="fas fa-ellipsis-h"></i>
                    </span>
                </a>
                <div class="navbar-dropdown bd-navbar-dropdown is-boxed">
                    <a class="navbar-item bd-is-hidden" href="{{ route('user.edit', $user) }}">
                        <div>
                            <div class="icon-text">
                                <span class="icon">
                                    <i class="fa fa-user"></i>
                                </span>
                                <span>
                                    <strong>My profile</strong>
                                </span>
                            </div>
                            Update and manage my information.
                        </div>
                    </a>
                    <hr class="navbar-divider"/>
                    <a class="navbar-item bd-is-hidden"
                       href="/logout">
                        <div>
                            <div class="icon-text">
                                <span class="icon has-text-danger">
                                    <i class="fas fa-sign-out-alt"></i>
                                </span>
                                <span class="has-text-danger">
                                    <strong>Log out</strong>
                                </span>
                            </div>

                            Hope to see you very soon ❤ !
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</nav>
