@extends('layout.app')

@section('content')

    <nav class="breadcrumb" aria-label="breadcrumbs">
        <ul>
            <li><a href="{{ route('home') }}">Home</a></li>
            <li class="is-active"><a href="#" aria-current="page">My profile</a></li>
        </ul>
    </nav>

    <h1 class="title">
        My Profile
    </h1>

    <h2 class="subtitle">
        Update your personal information.
    </h2>

    <div class="card my-5">
        <div class="card-content">
            <div class="content">
                <p>You don't have an account yet ?</p>
                <a href="/register" title="Create my account">Create your account.</a>
            </div>
        </div>
    </div>

    <form action="{{ route('user.update', $user) }}" method="POST">

        @csrf

        @method('PUT')

        <div class="field">
            <label class="label">Email address*</label>
            <div class="control">
                <input class="input" type="text" name="name" placeholder="Your name"
                       value="{{ old('name') ?? $user->getName() }}">
            </div>
            @error('name')
            <p class="help is-danger">{{ $message }}</p>
            @enderror
        </div>

        <div class="field">
            <label class="label">Email address*</label>
            <div class="control">
                <input class="input" type="email" name="email" placeholder="your@email.com"
                       value="{{ old('email') ?? $user->getEmail() }}">
            </div>
            @error('email')
            <p class="help is-danger">{{ $message }}</p>
            @enderror
        </div>

        <div class="field is-grouped">
            <div class="control">
                <button class="button is-link">Submit</button>
            </div>
        </div>
    </form>

@endsection
