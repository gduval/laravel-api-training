<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserEditRequest;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @param User $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(User $user): View{
        return view('user.edit', compact('user'));
    }

    /**
     * @param User $user
     * @param UserEditRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(User $user, UserEditRequest $request): \Illuminate\Http\RedirectResponse
    {
        $user->update($request->validated());
        $user->save();

        return redirect()->route('user.edit', $user)->with(['success' => 'Your profile has successfully been updated.']);
    }
}
