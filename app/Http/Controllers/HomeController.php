<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * @return View
     */
    public function index(): View {
        return view('home', [
            'user' => Auth::user()
        ]);
    }
}
